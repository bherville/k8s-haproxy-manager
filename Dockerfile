FROM golang:alpine



#
# Package Install
#

COPY ./src /src

RUN ls -la /src
WORKDIR /src/manager
RUN \
  go mod tidy \
  && go get bherville.com/dataplaneapi_sdk

RUN \
  GOOS=linux go build -o /app .



#
# Start App
#

ENTRYPOINT [ "go", "run", "." ]
