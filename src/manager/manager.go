/*
Copyright 2016 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Note: the example only works with the code within the same release/branch.
package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"gopkg.in/yaml.v2"

	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"

	haproxy_dataplaneapi "bherville.com/dataplaneapi_sdk"
)

type DataPlaneConfig struct {
	Scheme     string   `yaml:"scheme"`
	Host       string   `yaml:"host"`
	Port       int      `yaml:"port"`
	Sleep      int      `yaml:"sleep"`
	NodeLables []string `yaml:"nodeLables"`
}

type BackendServerConfig struct {
	Check    bool     `yaml:"check"`
	Maxconn  int64    `yaml:"maxconn"`
	Port     int64    `yaml:"port"`
	Weight   int64    `yaml:"weight"`
	Backends []string `yaml:"backends"`
}

func getDataPlaneConfig(configDir string) DataPlaneConfig {
	configFileContent, err := os.ReadFile(fmt.Sprintf("%v/%v", configDir, "haproxy-dataplane.yaml"))
	if err != nil {
		log.Fatal(err)
	}

	var config DataPlaneConfig

	err = yaml.Unmarshal(configFileContent, &config)
	if err != nil {
		panic(err)
	}

	return config
}

func getBackendServersConfig(configDir string) BackendServerConfig {
	configFileContent, err := os.ReadFile(fmt.Sprintf("%v/%v", configDir, "haproxy-backend-servers.yaml"))
	if err != nil {
		log.Fatal(err)
	}
	if err != nil {
		panic(err)
	}

	var config BackendServerConfig

	err = yaml.Unmarshal(configFileContent, &config)
	if err != nil {
		panic(err)
	}

	return config
}

func getDataPlane(dataPlaneConfig DataPlaneConfig, credDir string) haproxy_dataplaneapi.HAProxyDataPlane {
	usernameFileContent, err := os.ReadFile(fmt.Sprintf("%v/%v", credDir, "api-username"))
	if err != nil {
		log.Fatal(err)
	}

	passwordFileContent, err := os.ReadFile(fmt.Sprintf("%v/%v", credDir, "api-password"))
	if err != nil {
		log.Fatal(err)
	}

	auth := haproxy_dataplaneapi.HAProxyDataPlaneAuth{string(usernameFileContent), string(passwordFileContent)}

	return haproxy_dataplaneapi.HAProxyDataPlane{dataPlaneConfig.Scheme, dataPlaneConfig.Host, dataPlaneConfig.Port, auth}
}

func main() {
	fmt.Println("Starting up...")

	backendServersConfig := getBackendServersConfig(os.Getenv("DATAPLANE_CONFIG_DIR"))
	dataPlaneConfig := getDataPlaneConfig(os.Getenv("DATAPLANE_CONFIG_DIR"))
	dataPlane := getDataPlane(dataPlaneConfig, os.Getenv("DATAPLANE_CREDENTIALS_DIR"))

	// creates the in-cluster config
	config, err := rest.InClusterConfig()
	if err != nil {
		panic(err.Error())
	}
	// Create the client
	client, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}

	for {
		nodes, err := client.CoreV1().Nodes().List(context.TODO(), metav1.ListOptions{
			LabelSelector: strings.Join(dataPlaneConfig.NodeLables, ","),
		})
		if err != nil {
			panic(err.Error())
		}
		log.Printf("There are %d nodes in the cluster\n", len(nodes.Items))

		for _, backend := range backendServersConfig.Backends {
			log.Printf("Processing backend %s", backend)
			// Process each backend server
			for _, backendServer := range haproxy_dataplaneapi.GetBackendServers(dataPlane, backend).Servers {
				keepBackendServer := false

				for _, node := range nodes.Items {
					nodeAddress := node.Status.Addresses[1].Address

					if backendServer.Address == nodeAddress && backendServer.Name == node.Name {
						keepBackendServer = true
					}
				}

				if keepBackendServer == false {
					// Remove Server
					log.Printf("Backend server %s no longer exists, removing it from %s...\n", backendServer.Name, backend)
					haproxy_dataplaneapi.RemoveBackendServer(dataPlane, backend, backendServer, true)
				} else {
					log.Printf("Backend server %s still exists, keeping it in %s...\n", backendServer.Name, backend)
				}
			}

			// Process each node
			for _, node := range nodes.Items {
				nodeAddress := node.Status.Addresses[1].Address
				newServer := true

				for _, backendServer := range haproxy_dataplaneapi.GetBackendServers(dataPlane, backend).Servers {
					if backendServer.Address == nodeAddress && backendServer.Name == node.Name {
						newServer = false
					}
				}

				if newServer {
					// Add Server
					log.Printf("Backend server %s is new, adding it to %s...\n", node.Name, backend)
					log.Printf("config %s", backendServersConfig)
					haproxy_dataplaneapi.AddBackendServer(dataPlane, backend, haproxy_dataplaneapi.BackendServer{nodeAddress, haproxy_dataplaneapi.BackendServerCheck(haproxy_dataplaneapi.Enabled), backendServersConfig.Maxconn, node.Name, backendServersConfig.Port, backendServersConfig.Weight})
				} else {
					log.Printf("Backend server %s exists in %s, skipping it...\n", node.Name, backend)
				}
			}
		}

		// Check for errors
		if errors.IsNotFound(err) {
			log.Printf("Unabled to find any nodes\n")
		} else if statusError, isStatus := err.(*errors.StatusError); isStatus {
			log.Printf("Error getting pod %v\n", statusError.ErrStatus.Message)
		} else if err != nil {
			panic(err.Error())
		}

		time.Sleep(10 * time.Second)
	}
}
